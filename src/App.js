/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import { useEffect, useState, useRef, useCallback } from "react";
import PokemonCards from "./components/pokemon-cards/pokemon-cards";
import "./style.css";

function App() {
  const [pokemon, setPokemon] = useState([]);
  const [currentUrl, setCurrentUrl] = useState(
    "https://pokeapi.co/api/v2/pokemon"
  );
  const [nextUrl, setNextUrl] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    if (nextUrl !== null) {
      axios
        .get(currentUrl)
        .then((response) => {
          setNextUrl(response.data.next);

          setPokemon([...pokemon, ...response.data.results]);

          setLoading(false);
        })
        .catch((error) => {
          throw new Error(error);
        });
    }
  }, [currentUrl]);

  const observer = useRef();
  const lastPokeOnListRef = useCallback(
    (node) => {
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver((pokemon) => {
        if (pokemon[0].isIntersecting && nextUrl) {
          setCurrentUrl(nextUrl);
        }
      });
      if (node) observer.current.observe(node);
    },
    [nextUrl]
  );

  return (
    <div className="main-container">
      <h1 className="main-title">Lists of Pokemon</h1>
      <PokemonCards pokemon={pokemon} lastPokeOnListRef={lastPokeOnListRef} />
      {loading ? <p>Loading....</p> : null}
    </div>
  );
}

export default App;
