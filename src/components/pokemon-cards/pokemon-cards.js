import React from "react";
import "./cards-style.css";
const PokemonCard = React.lazy(() => import("../pokemon-card/pokemon-card"));

function PokemonCards({ pokemon, lastPokeOnListRef }) {
  return (
    <div className="cards-container">
      {pokemon.map((poke, index) => {
        if (pokemon.length === index + 1) {
          return (
            <div ref={lastPokeOnListRef} key={index} className="single-card">
              <PokemonCard singlePokemon={poke} />
            </div>
          );
        } else {
          return (
            <div key={index} className="single-card">
              <PokemonCard singlePokemon={poke} />
            </div>
          );
        }
      })}
    </div>
  );
}

export default PokemonCards;
