/* eslint-disable react-hooks/exhaustive-deps */
import axios from "axios";
import React, { useEffect, useState } from "react";
import Modal from "../poke-modal/modal";
import "./card-style.css";

function PokemonCard({ singlePokemon }) {
  const singlePokeUrl = singlePokemon.url;
  const [pokeData, setPokeData] = useState({});
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    if (showModal) {
      axios
        .get(singlePokeUrl)
        .then((response) => {
          setPokeData(response.data);
        })
        .catch((error) => {
          throw new Error(error);
        });
    }
  }, [showModal]);

  /**
   * Handle show/unshow modal
   */
  const handleModal = () => {
    setShowModal(!showModal);
  };

  return (
    <>
      <button type="button" onClick={handleModal} className="card-container">
        {singlePokemon.name}
      </button>
      {showModal ? (
        <Modal
          handleModal={handleModal}
          pokeData={pokeData}
          pokeName={singlePokemon.name}
        />
      ) : null}
    </>
  );
}

export default PokemonCard;
