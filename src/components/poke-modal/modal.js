import React from "react";
import "./modal.css";

function Modal({ handleModal, pokeData, pokeName }) {
  return (
    <div className="container">
      <div className="modal">
        <div className="modal-first-content">
          <div className="poke-image">
            <img
              src={pokeData?.sprites?.front_default}
              alt={`${pokeName} img`}
            />
          </div>

          <div className="poke-main-data">
            <div>
              <span>
                <b>ID: </b>
              </span>
              <span>#{pokeData?.id}</span>
            </div>
            <div className="poke-name">
              <span>
                <b>Name: </b>
              </span>
              <span>{pokeName}</span>
            </div>
            <div>
              <span>
                <b>Base exp: </b>
              </span>
              <span>{pokeData?.base_experience}</span>
            </div>
            <div>
              <span>
                <b>Weight: </b>
              </span>
              <span>{pokeData?.weight}</span>
            </div>
            <div>
              <span>
                <b>Height: </b>
              </span>
              <span>{pokeData?.height}</span>
            </div>
          </div>
        </div>

        <div className="modal-second-content">
          <div>
            <h4 className="title">Stats</h4>
            {pokeData?.stats?.map((data, index) => {
              return (
                <p key={index} className="info">
                  <b>{data.stat.name}</b> {data.base_stat}
                </p>
              );
            })}
          </div>

          <div>
            <h4 className="title">Types</h4>
            {pokeData?.types?.map((data, index) => {
              return (
                <p key={index} className="info">
                  {data.type.name}
                </p>
              );
            })}
          </div>

          <div>
            <h4 className="title">Abilities</h4>
            {pokeData?.abilities?.map((data, index) => {
              return (
                <p key={index} className="info">
                  {data.ability.name}
                </p>
              );
            })}
          </div>

          <div className="abilities-container">
            <h4 className="title">Moves</h4>
            <div className="abilities-lists">
              {pokeData?.moves?.map((data, index) => {
                return (
                  <p key={index} className="info">
                    {data.move.name}
                  </p>
                );
              })}
            </div>
          </div>
        </div>

        <button type="button" onClick={() => handleModal()}>
          X
        </button>
      </div>
    </div>
  );
}

export default Modal;
